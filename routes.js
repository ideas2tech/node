'use strict';

var userctl = require('./controllers/userctl');
module.exports = (router) => {
    router.post('/adduser', userctl.userdata);
}