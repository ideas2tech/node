'use strict';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 8085;
const mongo = require('./mongoconfig');
const path = require('path');

var router = express.Router();
mongo.testconn().then(() => {

    app.use(bodyParser.urlencoded({
        extended: true,
        limit: '500mb',
        parameterLimit: 50000
    }));
    app.use(bodyParser.json({ limit: '500mb' }));
    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*'); //Enable CORS
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Permissions');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        next();
    });
    require('./routes')(router);
    app.use('/api', router);
    app.set('view engine', 'pug');
    app.set(path.join(__dirname, 'views'));
}).then(() => {
    app.listen(port, () => {
        console.log('server running...' + port);
    });
}).catch((err) => {
    console.log(err);
});


